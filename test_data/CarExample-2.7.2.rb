project "CarExample", :version => "2.7.2", :original_filename => "car_example_2.7.2.mdzip", :generated_by => "plugin.modelGen-2.2.0", :generated_at => "2019-05-20 16:51:39 UTC", :additional_info => {:selected_model=>{:name=>"Application", :id=>"_17_0_2_4_b9202e7_1417447546518_583867_2182", :pluralize_role_names=>true, :snakecase_role_names=>true}}, :id => "_18_0_2_b9202e7_1557937965044_723592_4897" do
  model "Application", :imports => ["Automotive", "Geography", "Gui_Builder_Profile", "People"], :id => "_17_0_2_4_b9202e7_1417447546518_583867_2182" do
    applied_stereotype :instance_of => "Gui_Builder_Profile::change_tracked"
    applied_stereotype :instance_of => "Gui_Builder_Profile::options" do
      applied_tag :instance_of => "Gui_Builder_Profile::options::plugins", :value => "json_serializer"
    end
  end
  package "Automotive", :id => "_16_9_78e0236_1366066056048_138330_1616" do
    association :properties => ["_16_9_78e0236_1366068121348_662096_2317", "_16_9_78e0236_1366068121348_409936_2318"], :id => "_16_9_78e0236_1366068121348_380727_2316"
    association :properties => ["Automotive::Part::sub_parts", "Automotive::Part::part_of"], :id => "_17_0_2_4_5df01f7_1406214040752_778551_2320"
    association :properties => ["_16_9_78e0236_1366067397200_632446_2190", "Automotive::Mechanic::employer"], :id => "_16_9_78e0236_1366067397200_812133_2189"
    association :properties => ["_17_0_2_4_b9202e7_1398257214440_593947_2217", "_17_0_2_4_b9202e7_1398257214440_190360_2218"], :id => "_17_0_2_4_b9202e7_1398257214439_560992_2216"
    association :properties => ["Automotive::RepairShop::chief_mechanic", "Automotive::Mechanic::chief_for"], :id => "_16_9_78e0236_1367946780176_706893_2961"
    association :properties => ["Automotive::RepairShop::currently_working_on", "Automotive::Vehicle::being_repaired_by"], :id => "_16_9_78e0236_1366068307299_231485_2342"
    association :properties => ["Automotive::RepairShop::customers", "_16_9_78e0236_1366067360279_872653_2168"], :id => "_16_9_78e0236_1366067360279_940210_2166"
    association :properties => ["_16_9_78e0236_1366068093118_159859_2296", "_16_9_78e0236_1366068093119_394672_2297"], :id => "_16_9_78e0236_1366068093118_599748_2295"
    association :properties => ["_16_9_78e0236_1366067582868_23205_2244", "_16_9_78e0236_1366067582868_27841_2245"], :id => "_16_9_78e0236_1366067582868_511580_2243"
    association :properties => ["Automotive::Vehicle::maintained_by", "People::Person::maintains"], :id => "_16_9_78e0236_1366069282527_334070_2551"
    association :properties => ["Automotive::Vehicle::occupants", "People::Person::occupying"], :id => "_16_9_78e0236_1366069181789_345442_2528"
    association :properties => ["Automotive::VehicleTaxRate::for_country", "_16_9_78e0236_1366133428604_110816_2057"], :id => "_16_9_78e0236_1366133428604_876075_2055"
    association :properties => ["_16_9_78e0236_1366742974298_752804_2828", "_16_9_78e0236_1366742974298_565322_2829"], :id => "_16_9_78e0236_1366742974298_520143_2827"
    association :properties => ["Automotive::Warranty::replacement", "Automotive::Warrantied::replacement_for"], :id => "_17_0_2_4_b9202e7_1398699793004_636765_2510"
    klass "Car", :parents => ["Automotive::Vehicle"], :id => "_16_9_78e0236_1366066538607_984930_1890" do
      property "inspection_completed", :type => "UML::Boolean", :id => "_18_0_4_f920375_1490975790255_858268_4507"
      property "miles_per_gallon", :type => "UML::Integer", :id => "_16_9_78e0236_1366070835494_337831_1625"
      property "state", :type => "UML::String", :id => "_18_0_4_f920375_1490975779442_502870_4505"
    end
    klass "Component", :implements => ["Automotive::Warrantied"], :id => "_16_9_78e0236_1366068083951_709953_2270" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property nil, :type => "People::Ownership", :id => "_18_0_2_6340208_1467993281737_561226_4510"
      property nil, :type => "Automotive::Part", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366068121348_662096_2317"
      property nil, :type => "Automotive::Vehicle", :id => "_16_9_78e0236_1366068093119_394672_2297"
      property "name", :type => "UML::String", :id => "_16_9_78e0236_1366068097827_152417_2304"
    end
    klass "ElectricVehicle", :parents => ["Automotive::Vehicle"], :id => "_16_9_78e0236_1366070974876_297414_1657" do
      property "electric_efficiency", :type => "UML::Float", :id => "_16_9_78e0236_1366071072506_452961_1710"
      property "sponsor", :type => "Automotive::Sponsor", :id => "_16_9_78e0236_1366745991418_194966_2889"
    end
    klass "HybridVehicle", :parents => ["Automotive::Car", "Automotive::ElectricVehicle"], :id => "_16_9_78e0236_1366071031966_82236_1683" do
      property "hybrid_type", :type => "UML::String", :id => "_16_9_78e0236_1366071403745_137302_1758"
    end
    klass "Mechanic", :parents => ["People::Person"], :id => "_16_9_78e0236_1366066842109_319313_1954" do
      property "chief_for", :type => "Automotive::RepairShop", :lower => 1, :id => "_16_9_78e0236_1367946780176_549790_2963"
      property "employer", :type => "Automotive::RepairShop", :lower => 1, :id => "_16_9_78e0236_1366067397200_763212_2191"
      property "salary", :type => "Automotive::Salary", :id => "_16_9_78e0236_1366068210717_644624_2334"
    end
    klass "Minivan", :parents => ["Automotive::Car"], :id => "_16_9_78e0236_1366070892891_391515_1629" do
      property "sliding_doors", :type => "UML::Boolean", :id => "_16_9_78e0236_1366070914471_459575_1653"
    end
    klass "Motorcycle", :parents => ["Automotive::Vehicle"], :id => "_16_9_78e0236_1366067209021_8688_2102"
    klass "Part", :implements => ["Automotive::HasSerial"], :id => "_16_9_78e0236_1366068121339_953009_2314" do
      property nil, :type => "Automotive::Component", :id => "_16_9_78e0236_1366068121348_409936_2318"
      property "name", :type => "UML::String", :default_value => "n/a", :id => "_16_9_78e0236_1366068282038_331226_2336"
      property "part_of", :type => "Automotive::Part", :id => "_17_0_2_4_5df01f7_1406214040753_523052_2322"
      property "sub_parts", :type => "Automotive::Part", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_5df01f7_1406214040753_666131_2321"
    end
    klass "RepairShop", :id => "_16_9_78e0236_1366066811869_23985_1929" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property nil, :type => "Geography::Location", :id => "_17_0_2_4_b9202e7_1398257214440_593947_2217"
      property nil, :type => "Automotive::Mechanic", :upper => 5, :id => "_16_9_78e0236_1366067397200_632446_2190"
      property "chief_mechanic", :type => "Automotive::Mechanic", :lower => 1, :id => "_16_9_78e0236_1367946780176_953000_2962"
      property "currently_working_on", :type => "Automotive::Vehicle", :is_ordered => true, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366068307299_366847_2343"
      property "customers", :type => "People::Person", :upper => Float::INFINITY, :id => "_16_9_78e0236_1366067360279_92404_2167"
      property "name", :type => "UML::String", :id => "_16_9_78e0236_1366066822048_142177_1950"
    end
    klass "Repair_Workflow", :id => "_18_0_4_f920375_1490976375933_117450_4501" do
      property "project_name", :type => "UML::String", :id => "_18_0_2_b9202e7_1491226225280_872639_4527"
    end
    klass "VIN", :id => "_16_9_78e0236_1366067547953_336846_2214" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property nil, :type => "Automotive::Vehicle", :id => "_16_9_78e0236_1366067582868_27841_2245"
      property "issue_date", :type => "UML::Date", :id => "_16_9_78e0236_1366067555255_262474_2235"
      property "vin", :type => "UML::Integer", :id => "_16_9_78e0236_1366067565263_597596_2237"
    end
    klass "Vehicle", :implements => ["Automotive::Warrantied"], :id => "_16_9_78e0236_1366067167425_72606_2065", :is_abstract => true do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property nil, :type => "Automotive::Component", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366068093118_159859_2296"
      property nil, :type => "Automotive::VIN", :aggregation => :composite, :lower => 1, :id => "_16_9_78e0236_1366067582868_23205_2244"
      property "being_repaired_by", :type => "Automotive::RepairShop", :id => "_16_9_78e0236_1366068307299_525124_2344"
      property "cost", :type => "Automotive::Dollar", :id => "_16_9_78e0236_1366130986718_499868_1806"
      property "drivers", :type => "People::Person", :is_ordered => true, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366067180171_210074_2092"
      property "maintained_by", :type => "People::Person", :is_ordered => true, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366069282527_414032_2552"
      property "make", :type => "Automotive::VehicleMaker", :default_value => "Unknown", :id => "_16_9_78e0236_1366067237861_330953_2126"
      property "occupants", :type => "People::Person", :is_ordered => true, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366069181789_18852_2529"
      property "owners", :type => "People::Person", :lower => 1, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366068637162_602760_2386"
      property "registered_at", :type => "Geography::Address", :id => "_17_0_4_2_78e0236_1385495933620_220365_3413"
      property "vehicle_model", :type => "UML::String", :id => "_16_9_78e0236_1366067247030_125163_2128"
    end
    klass "VehicleTaxRate", :id => "_16_9_78e0236_1366130997066_196250_1810" do
      property "for_country", :type => "Geography::Country", :lower => 1, :id => "_16_9_78e0236_1366133428604_820918_2056"
      property "property_tax_rate", :type => "UML::Float", :default_value => 5.1, :id => "_16_9_78e0236_1366131578843_71313_1926"
      property "sales_tax_rate", :type => "UML::Float", :default_value => 10.5, :id => "_16_9_78e0236_1366131010198_296393_1831"
    end
    klass "Warranty", :id => "_16_9_78e0236_1366742963186_82892_2802" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property nil, :type => "Automotive::Warrantied", :upper => Float::INFINITY, :id => "_16_9_78e0236_1366742974298_565322_2829"
      property "coverage", :type => "UML::String", :id => "_16_9_78e0236_1366742980973_501404_2836"
      property "replacement", :type => "Automotive::Warrantied", :aggregation => :composite, :id => "_17_0_2_4_b9202e7_1398699793005_409466_2511"
    end
    enumeration "DomesticVehicleMaker", :parents => ["Automotive::VehicleMaker"], :id => "_16_9_78e0236_1366131274895_481127_1842" do
      property "value", :type => "UML::String"
      literal "Dodge", :id => "_16_9_78e0236_1366645543913_469022_2038"
      literal "Ford", :id => "_16_9_78e0236_1366131283946_372909_1862"
    end
    enumeration "ForeignVehicleMaker", :parents => ["Automotive::VehicleMaker"], :id => "_16_9_78e0236_1366131356058_14344_1871" do
      property "value", :type => "UML::String"
      literal "Honda", :id => "_16_9_78e0236_1366131389324_738845_1894"
      literal "Volvo", :id => "_16_9_78e0236_1366131392278_961877_1896"
    end
    enumeration "OtherVehicleMaker", :parents => ["Automotive::DomesticVehicleMaker", "Automotive::ForeignVehicleMaker"], :id => "_18_0_2_b9202e7_1425056397229_12221_4274" do
      property "value", :type => "UML::String"
      literal "Home Made", :id => "_16_9_78e0236_1366131426461_369056_1898"
      literal "Other", :id => "_16_9_78e0236_1366131515014_274941_1900"
      literal "Unknown", :id => "_16_9_78e0236_1366068826969_905979_2441"
    end
    enumeration "PaymentFrequency", :id => "_18_0_2_b9202e7_1429890260187_422957_4505" do
      property "value", :type => "UML::String"
      literal "bi-weekly", :id => "_18_0_2_b9202e7_1429890284355_956659_4524"
      literal "monthly", :id => "_18_0_2_b9202e7_1429890294917_531740_4526"
      literal "none", :id => "_18_0_2_b9202e7_1429890304868_545479_4528"
    end
    enumeration "Sponsor", :id => "_16_9_78e0236_1366132054999_297854_1942" do
      property "value", :type => "UML::String"
      literal "Other", :id => "_16_9_78e0236_1366132229692_175881_1983"
      literal "US Federal Governemnt", :id => "_16_9_78e0236_1366132074857_95517_1962"
    end
    enumeration "VehicleMaker", :id => "_16_9_78e0236_1366067259195_605991_2136" do
      property "value", :type => "UML::String"
    end
    interface "HasSerial", :id => "_16_9_78e0236_1366071220089_148146_1714" do
      property "serial", :type => "UML::Integer", :id => "_16_9_78e0236_1366071259241_348916_1736"
    end
    interface "HasWarranty", :id => "_16_9_78e0236_1366120891708_25777_1993", :is_abstract => true do
      property "warranty_expiration_date", :type => "UML::Date", :id => "_16_9_78e0236_1366120931656_964792_2015"
    end
    interface "Warrantied", :parents => ["Automotive::HasSerial", "Automotive::HasWarranty"], :id => "_16_9_78e0236_1366131559805_623897_1906" do
      property nil, :type => "Automotive::Warranty", :id => "_16_9_78e0236_1366742974298_752804_2828"
      property "replacement_for", :type => "Automotive::Warranty", :id => "_17_0_2_4_b9202e7_1398699793005_634875_2512"
      property "warranty_void", :type => "UML::Boolean", :id => "_16_9_78e0236_1366131707187_76813_1928"
    end
    primitive "Dollar", :parents => ["UML::Integer"], :id => "_18_0_2_b9202e7_1427304928405_208415_4406"
    primitive "Salary", :parents => ["Automotive::Dollar"], :id => "_18_0_2_b9202e7_1427305008063_920214_4431"
  end
  package "Geography", :id => "_16_9_78e0236_1366066294834_445484_1712" do
    association :properties => ["_16_9_78e0236_1366066359210_104529_1757", "_16_9_78e0236_1366066359210_282017_1758"], :id => "_16_9_78e0236_1366066359210_993957_1756"
    association :properties => ["Geography::Address::registered_vehicles", "Automotive::Vehicle::registered_at"], :id => "_17_0_4_2_78e0236_1385495933620_558642_3411"
    association :properties => ["_16_9_78e0236_1366066447444_366789_1840", "_16_9_78e0236_1366066447444_65563_1841"], :id => "_16_9_78e0236_1366066447444_653304_1839"
    association :properties => ["_16_9_78e0236_1366132937915_72233_2017", "_16_9_78e0236_1366132937915_128621_2018"], :id => "_16_9_78e0236_1366132937915_467538_2016"
    association :properties => ["_16_9_78e0236_1366066439294_528294_1831", "_16_9_78e0236_1366066439295_424847_1832"], :id => "_16_9_78e0236_1366066439294_123425_1830"
    association :properties => ["_16_9_78e0236_1366132959521_359911_2039", "_16_9_78e0236_1366132959521_356478_2040"], :id => "_16_9_78e0236_1366132959521_483704_2038"
    klass "Address", :id => "_16_9_78e0236_1366066233074_749821_1662" do
      property nil, :type => "Geography::City", :id => "_16_9_78e0236_1366066359210_104529_1757"
      property nil, :type => "People::Person", :id => "_16_9_78e0236_1366066261001_729475_1701"
      property "registered_vehicles", :type => "Automotive::Vehicle", :upper => Float::INFINITY, :id => "_17_0_4_2_78e0236_1385495933620_892356_3412"
      property "street_name", :type => "UML::String", :id => "_16_9_78e0236_1366066326103_985730_1725"
      property "street_number", :type => "UML::Integer", :id => "_16_9_78e0236_1366066333638_12375_1727"
    end
    klass "City", :implements => ["Geography::Location"], :id => "_16_9_78e0236_1366066348515_997087_1731" do
      property nil, :type => "Geography::Address", :upper => Float::INFINITY, :id => "_16_9_78e0236_1366066359210_282017_1758"
      property nil, :type => "Geography::State", :id => "_16_9_78e0236_1366066439295_424847_1832"
      property nil, :type => "Geography::Territory", :id => "_16_9_78e0236_1366132959521_356478_2040"
      property "founder", :type => "People::Person", :id => "_18_0_2_b9202e7_1549029745138_904807_5451"
    end
    klass "Country", :implements => ["Geography::Location"], :id => "_16_9_78e0236_1366066408437_960762_1769" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property nil, :type => "Geography::State", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366066447444_366789_1840"
      property nil, :type => "Geography::Territory", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366132937915_72233_2017"
      property nil, :type => "Automotive::VehicleTaxRate", :id => "_16_9_78e0236_1366133428604_110816_2057"
    end
    klass "State", :id => "_16_9_78e0236_1366066433277_320648_1809" do
      property nil, :type => "Geography::City", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366066439294_528294_1831"
      property nil, :type => "Geography::Country", :id => "_16_9_78e0236_1366066447444_65563_1841"
      property "name", :type => "UML::String", :id => "_16_9_78e0236_1366066460177_366742_1852"
    end
    klass "Territory", :id => "_16_9_78e0236_1366132937906_401986_2014" do
      property nil, :type => "Geography::City", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366132959521_359911_2039"
      property nil, :type => "Geography::Country", :id => "_16_9_78e0236_1366132937915_128621_2018"
      property "name", :type => "UML::String", :id => "_16_9_78e0236_1366132951603_998023_2032"
    end
    interface "Location", :id => "_17_0_2_4_b9202e7_1398257160365_480149_2186" do
      property nil, :type => "Automotive::RepairShop", :upper => Float::INFINITY, :id => "_17_0_2_4_b9202e7_1398257214440_190360_2218"
      property "name", :type => "UML::String", :id => "_16_9_78e0236_1366066465800_762051_1854"
    end
  end
  package "People", :id => "_16_9_78e0236_1366066083021_757821_1617", :documentation => "This is the documentation for module People." do
    association :properties => ["Automotive::Vehicle::owners", "People::Person::vehicles"], :association_class => "People::Ownership", :id => "ac__16_9_78e0236_1366068672650_433241_2402"
    association :properties => ["People::Ownership::purchased_components", "_18_0_2_6340208_1467993281737_561226_4510"], :id => "_18_0_2_6340208_1467993281737_304216_4508"
    association :properties => ["_16_9_78e0236_1366066261001_73022_1700", "_16_9_78e0236_1366066261001_729475_1701"], :id => "_16_9_78e0236_1366066261001_884941_1699"
    association :properties => ["People::Person::drives", "Automotive::Vehicle::drivers"], :association_class => "People::Driving", :id => "ac__16_9_78e0236_1366067180171_151003_2089"
    association :properties => ["People::Person::founded", "Geography::City::founder"], :association_class => "People::Founding", :id => "ac__18_0_2_b9202e7_1549029745138_612815_5449"
    klass "Driving", :id => "_16_9_78e0236_1366067180171_151003_2089" do
      property "car_review", :type => "Gui_Builder_Profile::RichText", :id => "_17_0_2_4_78e0236_1381864773281_380930_2188"
      property "likes_driving", :type => "UML::Boolean", :default_value => true, :id => "_16_9_78e0236_1366067180171_558861_2090"
    end
    klass "Founding", :id => "_18_0_2_b9202e7_1549029745138_612815_5449" do
      property "date", :type => "Gui_Builder_Profile::Date", :id => "_18_0_2_b9202e7_1549029899181_437371_5524"
    end
    klass "Ownership", :id => "_16_9_78e0236_1366068672650_433241_2402" do
      property "payment_frequency", :type => "Automotive::PaymentFrequency", :id => "_18_0_2_b9202e7_1429890367125_152646_4532"
      property "percent_ownership", :type => "UML::Float", :default_value => 100.0, :id => "_16_9_78e0236_1366068687479_98355_2438"
      property "purchased_components", :type => "Automotive::Component", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1467993281737_470396_4509"
    end
    klass "Person", :id => "_16_9_78e0236_1366066203528_181267_1642", :documentation => "This is the documentation for class People::Person." do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      property nil, :type => "Geography::Address", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366066261001_73022_1700"
      property nil, :type => "Automotive::RepairShop", :upper => Float::INFINITY, :id => "_16_9_78e0236_1366067360279_872653_2168"
      property "aliases", :type => "People::Alias", :upper => Float::INFINITY, :id => "_18_0_2_b9202e7_1549029305074_532640_4660"
      property "date_of_birth", :type => "Gui_Builder_Profile::Date", :id => "_16_9_78e0236_1366070298713_781654_2582"
      property "dependent", :type => "UML::Boolean", :id => "_16_9_78e0236_1367851658571_925977_2920"
      property "description", :type => "Gui_Builder_Profile::RichText", :id => "_16_9_78e0236_1366066730057_852526_1913"
      property "drives", :type => "Automotive::Vehicle", :is_ordered => true, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366067180171_831195_2091"
      property "family_size", :type => "UML::Integer", :default_value => 1, :id => "_16_9_78e0236_1366132675099_919360_1987"
      property "founded", :type => "Geography::City", :id => "_18_0_2_b9202e7_1549029745138_365918_5450"
      property "handedness", :type => "People::Handedness", :id => "_16_9_78e0236_1367851780220_478308_2952"
      property "last_updated", :type => "Gui_Builder_Profile::Timestamp", :id => "_16_9_78e0236_1366070312329_423993_2584"
      property "lucky_numbers", :type => "UML::Integer", :upper => Float::INFINITY, :id => "_18_0_2_b9202e7_1432215565975_569671_4503", :documentation => "Tests primitive attribute with multiplicity > 1"
      property "maintains", :type => "Automotive::Vehicle", :is_ordered => true, :upper => Float::INFINITY, :id => "_16_9_78e0236_1366069282528_643359_2553"
      property "manifesto", :type => "Gui_Builder_Profile::BigString", :id => "_18_0_2_b9202e7_1428077964784_863371_4517"
      property "name", :type => "People::PersonName", :id => "_16_9_78e0236_1366066727087_705277_1911"
      property "notes", :type => "Gui_Builder_Profile::RichText", :id => "_18_0_2_6340208_1541617452920_919544_4641"
      property "occupying", :type => "Automotive::Vehicle", :id => "_16_9_78e0236_1366069181790_824807_2530"
      property "photo", :type => "Gui_Builder_Profile::File", :id => "_16_9_78e0236_1366066742086_49756_1915"
      property "vehicles", :type => "Automotive::Vehicle", :upper => Float::INFINITY, :id => "_16_9_78e0236_1366068637162_794271_2387"
      property "wakes_at", :type => "Gui_Builder_Profile::Time", :id => "_18_0_2_b9202e7_1428078026581_201996_4519"
      property "weight", :type => "UML::Real", :id => "_16_9_78e0236_1367849816802_387872_2914"
    end
    enumeration "Handedness", :id => "_16_9_78e0236_1367851748736_975019_2926" do
      property "value", :type => "UML::String"
      literal "Ambidextrous", :id => "_16_9_78e0236_1367851772642_430102_2950"
      literal "Left Handed", :id => "_16_9_78e0236_1367851766866_441718_2948"
      literal "Right Handed", :id => "_16_9_78e0236_1367851751626_861818_2946"
    end
    package "Clown", :id => "_17_0_2_4_b9202e7_1396902886096_668173_2358" do
      association :properties => ["People::Clown::Clown::dolls", "_17_0_2_4_5df01f7_1406653717145_507_2207"], :id => "_17_0_2_4_5df01f7_1406653717145_329231_2205"
      association :properties => ["People::Clown::NestingDoll::inner_doll", "People::Clown::NestingDoll::outer_doll"], :id => "_17_0_2_4_5df01f7_1406653812657_961783_2240"
      association :properties => ["_18_0_4_f920375_1554157375938_451748_4917", "_18_0_4_f920375_1554157375938_713361_4918"], :association_class => "People::Clown::Riding", :id => "ac__18_0_4_f920375_1554157375937_928970_4916"
      klass "Clown", :parents => ["People::Person"], :id => "_17_0_2_4_b9202e7_1396902961765_481632_2378" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::root"
        property nil, :type => "People::Clown::Unicycle", :lower => 1, :upper => Float::INFINITY, :id => "_18_0_4_f920375_1554157375938_713361_4918"
        property "affiliations", :type => "People::Clown::ClownAffiliations", :upper => Float::INFINITY, :id => "_18_0_2_b9202e7_1425074930658_255989_4334"
        property "clown_name", :type => "UML::String", :id => "_17_0_2_4_b9202e7_1396903232189_233957_2460"
        property "dolls", :type => "People::Clown::NestingDoll", :upper => Float::INFINITY, :id => "_17_0_2_4_5df01f7_1406653717145_726818_2206"
      end
      klass "NestingDoll", :id => "_17_0_2_4_5df01f7_1406653695759_767940_2183" do
        property nil, :type => "People::Clown::Clown", :id => "_17_0_2_4_5df01f7_1406653717145_507_2207"
        property "color", :type => "UML::String", :id => "_17_0_2_4_5df01f7_1406653797512_635921_2235"
        property "inner_doll", :type => "People::Clown::NestingDoll", :aggregation => :composite, :id => "_17_0_2_4_5df01f7_1406653812657_218701_2241"
        property "outer_doll", :type => "People::Clown::NestingDoll", :id => "_17_0_2_4_5df01f7_1406653812657_717983_2242"
        property "traits", :type => "People::Clown::DollTraits", :upper => Float::INFINITY, :id => "_18_0_2_b9202e7_1426099796692_162004_4427"
      end
      klass "Riding", :id => "_18_0_4_f920375_1554157375937_928970_4916" do
        property "aquired", :type => "Gui_Builder_Profile::Date", :id => "_18_0_4_f920375_1554157646272_649223_4973"
        property "description", :type => "Gui_Builder_Profile::RichText", :id => "_18_0_4_f920375_1554212852170_778981_4980"
        property "is_favorite", :type => "UML::Boolean", :id => "_18_0_4_f920375_1554157696573_880733_4975"
        property "nickname", :type => "UML::String", :id => "_18_0_4_f920375_1554157728045_74981_4977"
      end
      klass "Unicycle", :parents => ["Automotive::Vehicle"], :id => "_17_0_2_4_b9202e7_1396902899128_227206_2359" do
        property nil, :type => "People::Clown::Clown", :id => "_18_0_4_f920375_1554157375938_451748_4917"
        property "color", :type => "UML::String", :id => "_17_0_2_4_b9202e7_1396903217791_755892_2458"
      end
      enumeration "ClownAffiliations", :id => "_18_0_2_b9202e7_1425074833663_66011_4309" do
        property "value", :type => "UML::String"
        literal "Bozos United", :id => "_18_0_2_b9202e7_1425074909854_641686_4332"
        literal "National Clown Association", :id => "_18_0_2_b9202e7_1425074895028_286947_4330"
        literal "World Clown Union", :id => "_18_0_2_b9202e7_1425074868662_238794_4328"
      end
      enumeration "DollTraits", :id => "_18_0_2_b9202e7_1426099664830_519591_4402" do
        property "value", :type => "UML::String"
        literal "Fat", :id => "_18_0_2_b9202e7_1426099736557_217379_4421"
        literal "Furry", :id => "_18_0_2_b9202e7_1426099758655_115054_4425"
        literal "Happy", :id => "_18_0_2_b9202e7_1426099749875_271184_4423"
      end
    end
    primitive "Alias", :parents => ["People::PersonName"], :id => "_18_0_2_b9202e7_1432223727401_848715_4527"
    primitive "PersonName", :parents => ["UML::String"], :id => "_18_0_2_b9202e7_1427305151846_608288_4459"
  end
  profile "Gui_Builder_Profile", :id => "_9_5_1_f2e0365_1132085180552_532309_18941" do
    applied_stereotype :instance_of => "Gui_Builder_Profile::options"
    association :properties => ["_17_0_4_2_78e0236_1386346399057_474802_3400", "Gui_Builder_Profile::File::binary_data"], :id => "_17_0_4_2_78e0236_1386346399057_554755_3399"
    association :properties => ["Gui_Builder_Profile::InvitationCode::additional_email_requirements", "Gui_Builder_Profile::StringCondition::additional_email_requirement_for"], :id => "_16_9_78e0236_1362444527650_795906_1494"
    association :properties => ["Gui_Builder_Profile::InvitationCode::roles_granted", "Gui_Builder_Profile::UserRole::invitations"], :association_class => "Gui_Builder_Profile::GrantedPermissions", :id => "ac__16_9_78e0236_1361655331501_974661_1486"
    association :properties => ["_18_0_2_6340208_1440773581304_321556_4138", "_18_0_2_6340208_1440773581305_932128_4139"], :id => "_18_0_2_6340208_1440773581304_487621_4137"
    association :properties => ["_18_0_2_6340208_1437670247479_848660_4283", "_18_0_2_6340208_1437670247479_258317_4284"], :id => "_18_0_2_6340208_1437670247479_960929_4282"
    association :properties => ["_16_9_78e0236_1362000053868_346349_1709", "_16_9_78e0236_1362000053869_30585_1710"], :id => "_16_9_78e0236_1362000053868_440794_1708"
    association :properties => ["Gui_Builder_Profile::Perspective::substitutions", "_16_9_78e0236_1361999994974_761045_1678"], :id => "_16_9_78e0236_1361999994973_620448_1676"
    association :properties => ["Gui_Builder_Profile::ProjectOptions::email_requirements", "Gui_Builder_Profile::StringCondition::email_requirement_for"], :id => "_16_9_78e0236_1362435833928_585337_1539"
    association :properties => ["Gui_Builder_Profile::ProjectOptions::password_requirements", "Gui_Builder_Profile::StringCondition::password_requirement_for"], :id => "_16_9_78e0236_1362435861830_956872_1548"
    association :properties => ["Gui_Builder_Profile::RichText::images", "_17_0_4_2_78e0236_1386346294429_948280_3299"], :id => "_17_0_4_2_78e0236_1386346294427_421114_3296"
    association :properties => ["_16_9_78e0236_1362000016908_686909_1688", "_16_9_78e0236_1362000016909_933287_1689"], :id => "_16_9_78e0236_1362000016908_599111_1687"
    association :properties => ["Gui_Builder_Profile::User::roles", "Gui_Builder_Profile::UserRole::users"], :association_class => "Gui_Builder_Profile::RolePermissions", :id => "ac__16_9_78e0236_1361377547806_696994_1711"
    klass "BinaryData", :id => "_16_9_28b0142_1350320910064_324080_1403" do
      property nil, :type => "Gui_Builder_Profile::File", :id => "_17_0_4_2_78e0236_1386346399057_474802_3400"
      property "data", :type => "UML::ByteString", :id => "_16_9_28b0142_1350322818987_206540_1405"
    end
    klass "Code", :id => "_16_9_28b0142_1305737761267_4412_1473" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "content", :type => "UML::String", :id => "_16_9_28b0142_1345232682459_819388_1563"
      property "language", :type => "Gui_Builder_Profile::LanguageType", :id => "_16_9_28b0142_1305737768320_72319_1474"
    end
    klass "File", :id => "_16_9_6620216_1310677254545_658641_1463" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "binary_data", :type => "Gui_Builder_Profile::BinaryData", :aggregation => :composite, :id => "_16_9_6620216_1310677254546_719122_1464"
      property "filename", :type => "UML::String", :id => "_16_9_6620216_1310677254546_312818_1465"
      property "mime_type", :type => "UML::String", :id => "_16_9_6620216_1310677254546_544129_1466"
    end
    klass "GrantedPermissions", :id => "_16_9_78e0236_1361655331501_974661_1486" do
      property "role_manager", :type => "UML::Boolean", :id => "_16_9_78e0236_1361655366955_945029_1526"
    end
    klass "InvitationCode", :id => "_16_9_78e0236_1361654692810_715969_1432" do
      property nil, :type => "Gui_Builder_Profile::Organization", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1440773581305_932128_4139"
      property nil, :type => "Gui_Builder_Profile::Perspective", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000053869_30585_1710"
      property "additional_email_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1362444527651_178908_1495"
      property "code", :type => "UML::String", :id => "_16_9_78e0236_1361654722176_380927_1450"
      property "expires", :type => "Gui_Builder_Profile::Timestamp", :id => "_16_9_78e0236_1361654732105_93745_1452"
      property "roles_granted", :type => "Gui_Builder_Profile::UserRole", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361654888054_544647_1461"
      property "uses_remaining", :type => "UML::Integer", :id => "_16_9_78e0236_1361654751742_918976_1454"
    end
    klass "MultivocabularySubstitution", :id => "_16_9_6620216_1311349467640_10706_1828" do
      property nil, :type => "Gui_Builder_Profile::Perspective", :lower => 1, :id => "_16_9_78e0236_1361999994974_761045_1678"
      property "master_word", :type => "UML::String", :id => "_16_9_6620216_1311349486021_149646_1829"
      property "replacement_word", :type => "UML::String", :id => "_16_9_6620216_1311349518319_893941_1830"
    end
    klass "Organization", :id => "_18_0_2_6340208_1437670007691_142464_4253" do
      property nil, :type => "Gui_Builder_Profile::InvitationCode", :aggregation => :shared, :upper => Float::INFINITY, :id => "_18_0_2_6340208_1440773581304_321556_4138"
      property nil, :type => "Gui_Builder_Profile::Person", :aggregation => :shared, :upper => Float::INFINITY, :id => "_18_0_2_6340208_1437670247479_848660_4283"
      property "description", :type => "Gui_Builder_Profile::RichText", :id => "_18_0_2_6340208_1437670022410_877209_4276"
      property "name", :type => "UML::String", :id => "_18_0_2_6340208_1437670018818_673294_4274"
      property "org_user_limit", :type => "UML::Integer", :default_value => 2, :id => "_18_0_4_f920375_1482167552396_757856_4251"
    end
    klass "Person", :id => "_17_0_2_4_b9202e7_1402410462540_930340_2171" do
      property nil, :type => "Gui_Builder_Profile::Organization", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1437670247479_258317_4284"
      property "email", :type => "UML::String", :id => "_16_9_78e0236_1361378907971_285264_1915"
      property "email_verified", :type => "UML::Boolean", :id => "_16_9_78e0236_1361378935636_408890_1917"
      property "first_name", :type => "UML::String", :id => "_17_0_2_4_b9202e7_1402410477887_979887_2190"
      property "last_name", :type => "UML::String", :id => "_17_0_2_4_b9202e7_1402410490650_559652_2192"
    end
    klass "Perspective", :id => "_16_9_78e0236_1361999978172_908613_1658" do
      property nil, :type => "Gui_Builder_Profile::InvitationCode", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000053868_346349_1709"
      property nil, :type => "Gui_Builder_Profile::User", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000016909_933287_1689"
      property "name", :type => "UML::String", :id => "_16_9_78e0236_1362000125819_873673_1725"
      property "substitutions", :type => "Gui_Builder_Profile::MultivocabularySubstitution", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1361999994973_454878_1677"
    end
    klass "ProjectOptions", :id => "_16_9_78e0236_1361379064628_70918_1919" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::singleton"
      property "email_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1362435833929_936829_1540"
      property "password_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1362435861830_763541_1549"
      property "user_registration", :type => "Gui_Builder_Profile::UserRegistrationType", :default_value => "Open", :id => "_16_9_78e0236_1361379249201_791722_1950"
    end
    klass "RegularExpressionCondition", :parents => ["Gui_Builder_Profile::StringCondition"], :id => "_16_9_78e0236_1362435709826_262438_1513" do
      property "regular_expression", :type => "Gui_Builder_Profile::RegularExpression", :id => "_16_9_78e0236_1362435738776_707179_1516"
    end
    klass "RichText", :id => "_17_0_4_2_78e0236_1386346294425_836962_3291" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "content", :type => "UML::String", :id => "_17_0_4_2_78e0236_1386346294426_434823_3293"
      property "images", :type => "Gui_Builder_Profile::RichTextImage", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_4_2_78e0236_1386346294428_392424_3297"
      property "markup_language", :type => "Gui_Builder_Profile::MarkupType", :id => "_17_0_4_2_78e0236_1386346294427_626186_3294"
    end
    klass "RichTextImage", :id => "_17_0_4_2_78e0236_1386346294425_790237_3292" do
      property nil, :type => "Gui_Builder_Profile::RichText", :id => "_17_0_4_2_78e0236_1386346294429_948280_3299"
      property "image", :type => "Gui_Builder_Profile::File", :id => "_17_0_4_2_78e0236_1386346294427_443952_3295"
    end
    klass "RolePermissions", :id => "_16_9_78e0236_1361377547806_696994_1711" do
      property "role_manager", :type => "UML::Boolean", :id => "_16_9_78e0236_1361377597251_847958_1743"
    end
    klass "StringCondition", :id => "_16_9_78e0236_1362435638076_723054_1493", :is_abstract => true do
      property "additional_email_requirement_for", :type => "Gui_Builder_Profile::InvitationCode", :id => "_16_9_78e0236_1362444527652_678514_1496"
      property "description", :type => "UML::String", :id => "_16_9_78e0236_1362439385411_917299_1696"
      property "email_requirement_for", :type => "Gui_Builder_Profile::ProjectOptions", :id => "_16_9_78e0236_1362435833929_239766_1541"
      property "failure_message", :type => "UML::String", :id => "_16_9_78e0236_1362435763496_769708_1518"
      property "password_requirement_for", :type => "Gui_Builder_Profile::ProjectOptions", :id => "_16_9_78e0236_1362435861830_154310_1550"
    end
    klass "User", :parents => ["Gui_Builder_Profile::Person"], :id => "_16_9_6620216_1311349088211_41010_1823" do
      property nil, :type => "Gui_Builder_Profile::Perspective", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000016908_686909_1688"
      property "email_confirmation_token", :type => "UML::String", :id => "_18_0_2_f920375_1426774368237_166134_4251"
      property "login", :type => "UML::String", :id => "_16_9_6620216_1311349134111_355250_1824"
      property "password_hash", :type => "UML::String", :id => "_16_9_6620216_1311349178120_340215_1825"
      property "password_reset_time_limit", :type => "Gui_Builder_Profile::Timestamp", :id => "_18_0_2_f920375_1426623768994_451284_4139"
      property "password_reset_token", :type => "UML::String", :id => "_18_0_2_f920375_1426623641990_599140_4137"
      property "roles", :type => "Gui_Builder_Profile::UserRole", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361377541576_508361_1703"
      property "salt", :type => "UML::String", :id => "_16_9_6620216_1311349196390_496663_1826"
      property "use_accessibility", :type => "UML::Boolean", :id => "_16_9_6620216_1311349222322_790429_1827"
    end
    klass "UserRole", :id => "_16_9_6620216_1311349580039_47554_1831" do
      property "invitations", :type => "Gui_Builder_Profile::InvitationCode", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361654888054_232347_1462"
      property "name", :type => "UML::String", :id => "_16_9_6620216_1311349591342_46442_1832"
      property "registration", :type => "Gui_Builder_Profile::RoleRegistrationType", :default_value => "Role Manager", :id => "_16_9_78e0236_1361377688965_687613_1769"
      property "users", :type => "Gui_Builder_Profile::User", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361377541576_939654_1704"
    end
    enumeration "FacadeImplementationLanguages", :id => "_16_8_6620216_1283178501783_51776_1249" do
      property "value", :type => "UML::String"
      literal "Java", :id => "_16_8_6620216_1283178557628_106569_1250"
      literal "Ruby", :id => "_16_8_6620216_1283178566324_155772_1251"
    end
    enumeration "LanguageType", :id => "_16_9_28b0142_1305737723715_669375_1467" do
      property "value", :type => "UML::String"
      literal "Clojure", :id => "_16_9_28b0142_1305737723716_562372_1470"
      literal "Groovy", :id => "_16_9_28b0142_1305737723716_180112_1471"
      literal "Javascript", :id => "_16_9_28b0142_1305737723716_116462_1469"
      literal "Python", :id => "_16_9_28b0142_1305737723717_525399_1472"
      literal "Ruby", :id => "_16_9_28b0142_1305737723715_442867_1468"
    end
    enumeration "MarkupType", :id => "_17_0_4_2_78e0236_1386346253949_528969_3262" do
      property "value", :type => "UML::String"
      literal "HTML", :id => "_17_0_4_2_78e0236_1386346253951_61218_3266"
      literal "Kramdown", :id => "_18_0_2_6340208_1461702006551_892495_4251"
      literal "LaTeX", :id => "_17_0_4_2_78e0236_1386346253951_848506_3264"
      literal "Markdown", :id => "_17_0_4_2_78e0236_1386346253951_125387_3263"
      literal "Plain", :id => "_17_0_4_2_78e0236_1386346253951_470997_3265"
      literal "Textile", :id => "_17_0_4_2_78e0236_1386346253951_18150_3267"
    end
    enumeration "RoleRegistrationType", :id => "_16_9_78e0236_1361377755013_916086_1790" do
      property "value", :type => "UML::String"
      literal "Administrator", :id => "_16_9_78e0236_1361377828577_385709_1816"
      literal "Open", :id => "_16_9_78e0236_1361377757085_493086_1808"
      literal "Role", :id => "_16_9_78e0236_1361377761232_512485_1810"
      literal "Role Manager", :id => "_16_9_78e0236_1361377824614_582502_1814"
    end
    enumeration "UserRegistrationType", :id => "_16_9_78e0236_1361379303537_735379_1975" do
      property "value", :type => "UML::String"
      literal "Email", :id => "_16_9_78e0236_1361379315080_235305_1995"
      literal "Invitation", :id => "_16_9_78e0236_1361379375691_302758_1997"
      literal "Open", :id => "_16_9_78e0236_1361379305457_933520_1993"
    end
    primitive "BigString", :parents => ["UML::String"], :id => "_18_0_2_b9202e7_1428075370972_160109_4275"
    primitive "Date", :id => "_18_0_2_b9202e7_1428076910048_980742_4348"
    primitive "RegularExpression", :parents => ["UML::String"], :id => "_16_9_78e0236_1361379717284_94123_2076"
    primitive "Time", :id => "_18_0_2_b9202e7_1428076858153_303327_4329"
    primitive "Timestamp", :id => "_18_0_2_b9202e7_1428076787996_461235_4311"
    stereotype "change_tracked", :metaclasses => ["Package"], :id => "_16_9_28b0142_1320261650047_288289_1577"
    stereotype "complex_attribute", :metaclasses => ["Class"], :id => "_16_9_28b0142_1350587904204_746275_1536"
    stereotype "facade", :metaclasses => ["Interface"], :id => "_16_6_2_28b0142_1276094884676_922858_351"
    stereotype "options", :metaclasses => ["Package"], :id => "_16_9_28b0142_1284660904944_537040_1463" do
      tag "application_options", :id => "_18_0_2_b9202e7_1491226601808_704924_4254"
      tag "plugins", :id => "_18_0_2_b9202e7_1491226308408_96569_4251", :documentation => "A list of additional plugins that the generated code should include in the generated models.  Example: the inclusion of the :json_serializer plugin for Ruby Sequel models.  This string should be a list of plugins that are either comma, semicolon, or space seperated."
      tag "project_name", :id => "_16_9_78e0236_1366130322652_436960_1623"
      tag "require", :id => "_16_9_28b0142_1284660999929_840326_1469"
      tag "version", :id => "_16_9_6620216_1311350018593_840420_1847"
    end
    stereotype "root", :metaclasses => ["Class"], :id => "_16_6_2_28b0142_1276094884676_130193_349" do
      tag "Root Name", :id => "_16_6_2_28b0142_1276094884694_127513_360"
    end
    stereotype "singleton", :metaclasses => ["Class"], :id => "_16_9_78e0236_1361379210582_818602_1938"
  end
end
