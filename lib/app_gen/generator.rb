require 'file_hierarchy_builder/builder'
require 'appellation'
require 'uml_metamodel'
require 'uml_metamodel/dsl'
require 'spec_gen'
require 'sequel_gen'
require 'fileutils'


class UmmAppGenerator
  def self.app_spec
    { :gem_name => {
        'specs'      => {
          'placeholder.txt' => 'placeholder.txt' 
        },
        # 'features' => { 'placeholder.txt' => 'placeholder.txt' },
        # Manually make a test directory if Test::Unit is required
        'bin'        => {
          # Want 'u+x', but it's only accepting integers. Leading 0 means Octal 
          ['gem_name_irb', :downcaseFirst]     => ['bin/x_irb.erb',    [:chmod, 0755, :FILE_PATH] ],
          ['gem_name_backup', :downcaseFirst]  => ['bin/x_backup.erb', [:chmod, 0755, :FILE_PATH] ],
          ['gem_name_import', :downcaseFirst]  => ['bin/x_import.erb', [:chmod, 0755, :FILE_PATH] ],
          ['gem_name_restore', :downcaseFirst] => ['bin/x_restore.erb', [:chmod, 0755, :FILE_PATH] ],
          ['gem_name_start_gui_server', :downcaseFirst] => ['bin/x_start_gui_server.erb', [:chmod, 0755, :FILE_PATH] ],
          ['gem_name_start_obj_server', :downcaseFirst] => ['bin/x_start_obj_server.erb', [:chmod, 0755, :FILE_PATH] ]
        },
        'lib'        => {
          ['gem_name.rb', :downcaseFirst] => nil, 
          :gem_name => {
            'meta_info.rb' => 'lib/x/meta_info.erb'#,
            # 'business_rules.rb' => nil,
            # 'custom_spec.rb' => :'lib/x/custom_spec.rb',
            # 'model_extensions.rb' => 'lib/x/model_extensions.erb',
            # 'pages' => {
            #   'controller' => { 'render.rb' => 'lib/x/pages/controller/render.erb'},
            #   'view' => {
            #   'render' => {
            #     'document_select.haml' => 'lib/x/pages/view/render/document_select.erb',
            #     'project_name_content.haml' => 'lib/x/pages/view/render/x_content.erb'
            #   }
            # }
          # }
          } 
        },
        'doc_source' => {
          'LEGAL.textile'  => :'doc_source/LEGAL.textile', 
          'README.textile' => :'doc_source/README.textile',
          'img' => { 'placeholder.txt' => 'placeholder.txt' } 
        },
        'data'       => {
          'test_input'     => { 'placeholder.txt' => 'placeholder.txt' },
          'test_expected'  => { 'placeholder.txt' => 'placeholder.txt' } 
        }
      }
    }
  end
  
  def self.code_directory
    path = PrometheusFiles.root_dir
    mkdir_p(path) unless File.exists?(path)
    raise "#{path} already exists, but is not a directory" unless File.directory?(path)
    path
  end

  def self.construct_app_from_file(path)
    construct_app(UmlMetamodel.from_dsl_file(path), path)
  end

  def self.construct_app(project, dsl_file = nil)
    message = ""
    begin
      load 'sequel_gen.rb' # load, not require!
      SequelBuilder.generate_ruby_sequel_from_umm_project(project)
      load 'spec_gen.rb' # load, not require!
      SpecBuilder.generate_gui_spec_from_umm_project(project)
    rescue Exception => e
      message << "\nFailure in code generation!!"
      puts "Failure in code generation!!"
      raise e
    else
      Dir.chdir code_directory
      gem_name      = Appellation.gem_name(project)
      model_version = project.version
      value_hash    = {:gem_name => gem_name, :project => project, :version => model_version}
      # puts "code_directory: #{code_directory.inspect}, value_hash: #{value_hash.inspect}"
      Hierarchy_Builder.template_dir_path = File.join(File.dirname(__FILE__), 'templates')
      Hierarchy_Builder.make_hierarchy(code_directory, app_spec, value_hash)
      dsl_filename = "#{project.name}#{('-' + project.version) if project.version}.rb"      
      if dsl_file && File.exist?(File.expand_path(dsl_file))
        dsl_copy_path = File.join(PrometheusFiles.root_dir, gem_name + '_generated', dsl_filename)
        FileUtils.cp(File.expand_path(dsl_file), dsl_copy_path)
      end      
      puts Rainbow("\n=== APP GENERATION COMPLETE ===").orange
      puts Rainbow("""
      Now package the gems using MM (`gem install MM` first if you haven't already):
      `cd #{code_directory}/#{gem_name}_generated; package`
      `cd #{code_directory}/#{gem_name}; package`
      After this you may install the gems in the gemset of your choosing.
      """).cyan
    end
    puts message
    message
  end
end
