module UmmAppGen
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb  
  
  # Required
  GEM_NAME = "umm_app_gen"
  VERSION  = '0.0.3'
  SUMMARY  = %q{Generates application}
  AUTHORS  = ["Michael Faughn"]
  
  # Optional
  EMAILS      = ["m.faughn@prometheuscomputing.com"]
  DESCRIPTION = %q{This creates the 'wrapper project' that ties together the generated code, object server, backup/restore, etc.
    Normally this should only be be used once per project, because you will will want to domain specific code
    (such as import code or rendering code) in this project, and you don't want to overwrite it.'}

  LANGUAGE         = :ruby # the language the project is written in
  LANGUAGE_VERSION = ['>= 2.3']
  RUNTIME_VERSIONS = { :mri => ['>= 2.3'] }

  DEPENDENCIES_RUBY = {
    :file_hierarchy_builder => '~> 1.0',
    :umm_sequel_gen         => '~> 0.0',
    :umm_gui_spec_gen       => '~> 0.0',
    :appellation            => '~> 0.0',
    :common                 => '~> 1.11' # really should be picked up by appellation but it isn't
  }
  DEVELOPMENT_DEPENDENCIES_RUBY = { :lodepath => '~> 0.0' }
  
end
