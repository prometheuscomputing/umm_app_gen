
#  NOTE  This is built on top of the DSL in gui_director/lib/gui_director/dsl/document_dsl.rb
#        gui_builder is obsolete

# One based
class ID_Gen
  def self.next
    last = @previous_number ||= 0
    current = last += 1
    @previous_number = current
    "Id_#{current}".to_sym
  end
end


class Object
    def human_readable_class_name
      self.class.to_title
    end
end


BULKY_WIDGET_TYPES = [:rch, :tbl, :lst, :obj, :dif]

# Intention of last_widget_type is to let you inject extra space between
# two types of widget on a case-by-case basis (kind of like kerning).
class Document_Spacing_Context
  def self.reset; @last_widget_type=nil; end
  def self.last_widget_type; @last_widget_type||=nil; end
  def self.last_widget_type=(symbol); @last_widget_type=symbol; end
  def self.bulky_last_widget?(*bulky_types)
    bulky_types = BULKY_WIDGET_TYPES if bulky_types.empty?
    bulky_types.member? last_widget_type
  end
end

module Gui::TopLevelDsl

  def self.doc(cls, title, &block)
    # id = cls.name.downcase
    # name = id.capitalize.to_sym
    id = title
    name = title.to_sym
    Document_Spacing_Context.reset
    document(name, cls, :identifier => id, :title => title, &block)
  end

end


module Gui::DocumentDsl

=begin
  Example divider attribute_strings
  These examples are from https://codepen.io/ibrahimjabbari/pen/ozinB

   # Color, height, margin, width: 'style="color:blue; margin-left:30px; height: 5px; width: 80%"'
   # Double: 'style="border-top: 3px double #8c8b8b;"'
   # Dashed: 'style="border-top: 1px dashed #8c8b8b;"'
   # Dotted: 'style="border-top: 1px dotted #8c8b8b;"'
   # Vertical gradient: 'style="height: 10px; width: 80%; box-shadow: 0 10px 10px -10px #8c8b8b inset"'
   # Horizontal gradient:
   'style="border: 0; height: 1px;
     background-image: -webkit-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
     background-image: -moz-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
     background-image: -ms-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
     background-image: -o-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); "'
=end
  def divider(attribute_string='')
    html(:label => 'Activity divider', :html => proc{ |junque|
    "<HR #{attribute_string}>" })
  end


=begin

  Sam tells me this won't work because the doc_depth and doc_index will be stored at each individual level.

  def doc_depth; @doc_depth||=0; end
  def doc_root?; 0==doc_depth; end
  def doc_depth_increment; @doc_depth=1+doc_depth; end
  def doc_depth_decrement
    newdepth = doc_depth - 1
    if doc_depth < 0
      puts ">>>>>>>> UNBALANCED doc_depth stack! <<<<<<<<<<<<<"
      new_depth = 0
    end
    @doc_depth = new_depth
  end

  # Table and List are both handled through association
  alias association original_association
  def association(getter, view_name, args = {}, &block)
    doc_depth_increment
    original_association(getter, view_name, args, &block)
    doc_depth_decrement
  end

  alias heading2 old_heading2
  def heading2(size, string, options = {}, &block)
    idx = @doc_index ||= Array.new
    idx << [size, string]
    puts "*** Heading (level #{size}): #{string.inspect}"
    old_heading2(size, string, options, &block)
  end
=end

=begin
  def google_map(getter, label, widget_name, array_of_points)
    html(:getter => getter, :label => label, :html => proc{ |data_json|

      locals = {data_json: data_json}
      haml = <<-EOS
      %div.#{widget_name}_cls##{widget_name}_widget
      :javascript
        initialize_sortable_widgets('#{widget_name}');
        update_tree('#{widget_name}');
      EOS
      Haml::Engine.new(haml.reset_indentation).render(self, locals)
    })
    newline; newline
  end
=end

  def tree_gor(getter, label, widget_name, unused_axes, default_axis_sequence)
    # unused_axes << 'Nothing'
    html(:getter => getter, :label => label, :html => proc{ |data_json|
      unused_axis_items = unused_axes.collect {|axis|
        "                  %li.ui-state-default #{axis}"
      }
      default_sequence_items = default_axis_sequence.collect {|axis|
        "                  %li.ui-state-default #{axis}"
      }
      locals = {data_json: data_json}
      #     %label.widget_label.main_input{for: "#{widget_name}_dropdown_id"}#{selector_label}
      haml = <<-EOS
      %div.input
        %table.treeselector.document_association_content_data.content_data
          %tbody
            %tr.header_row
              %th Representation
              %th Available Axes
              %th Axis Sequence
            %tr
              %td
                %select.widget_data##{widget_name}_dropdown_id{style: 'align-items: flex-start;', initial_value: 'Radial Tree', name: "#{widget_name}_dropdown_id", onchange: "update_tree('#{widget_name}')"}
                  %option{value: 'radial_reingold_tilford'}Radial Tree
                  %option{value: 'zoomTree'}Zoom Tree
                  %option{value: 'sunburst'}Drilldown Sunburst
              %td
                %ul##{widget_name}_available_id.sortable.#{widget_name}_dnd#{"\n"+unused_axis_items.join("\n")+"\n"}
              %td
                %ul##{widget_name}_sequence_id.sortable.#{widget_name}_dnd#{"\n"+default_sequence_items.join("\n")+"\n"}
          &nbsp;
        %input##{widget_name}_data_id{type: 'hidden', value: data_json}
        %div.#{widget_name}_cls##{widget_name}_widget
        :javascript
          initialize_sortable_widgets('#{widget_name}');
          update_tree('#{widget_name}');
      EOS
      Haml::Engine.new(haml.reset_indentation).render(self, locals)
    })
    newline; newline
  end



  def xy(getter, label, selector_label, widget_name)
    html(:getter => getter, :label => label, :html => proc{ |data_json|
      locals = {data_json: data_json}
      haml = <<-EOS
        %input##{widget_name}_data_id{type: 'hidden', value: data_json}
        %div.#{widget_name}_cls##{widget_name}_widget
          &nbsp;
        :javascript
          update_xy('#{widget_name}')
      EOS
      Haml::Engine.new(haml.reset_indentation).render(self, locals) rescue ''
    })
  end

  def sankey(getter, label, widget_name)
    html(:getter => getter, :label => label, :html => proc{ |data_json|
      locals = {data_json: data_json}
      haml = <<-EOS
        %input##{widget_name}_data_id{type: 'hidden', value: data_json}
        %div.#{widget_name}_cls##{widget_name}_widget
          &nbsp;
        :javascript
          update_sankey('#{widget_name}')
      EOS
      Haml::Engine.new(haml.reset_indentation).render(self, locals) rescue ''
    })
  end


  # The following three methods rely on module-side attributes, and are not thread safe.
  # It's not clear to me where they should instead cache their state.
  # Perhaps a thread local.
  def gui_stack
    @@gui_stack ||= Array.new
  end

  def newline_if_last_wiget_was_bulky(*types)
    newline if Document_Spacing_Context.bulky_last_widget?
  end

  def table?; :table==gui_stack.last; end
  def list?; :list==gui_stack.last; end

  def generate_label(getter)
    getter.capitalize.gsub('_', ' ')
  end

=begin   # Older, non collapsable code

  # Block does not take an argument.
  def tbl(getter, label=nil, args = {}, &block)
    label = generate_label(getter) unless label
    if !table?
      n = 2+gui_stack.size
      heading2(n, label, {})
    end
    gui_stack << :table
    args = args.clone
    args[:label]= label
    args[:title]||= args[:help]
    guidance=args.delete(:guidance)
    (write guidance; newline) if guidance
    table(getter, ID_Gen.next, args, &block)
    Document_Spacing_Context.last_widget_type= :tbl
    gui_stack.pop
    if !table?
      newline; newline
    end
  end

  # Block does not take an argument.
  def lst(getter, label=nil, args = {}, &block)
    label = generate_label(getter) unless label
    if !table?
      n = 2+gui_stack.size
      heading2(n, label, {})
    end
    gui_stack << :list
    args = args.clone
    args[:label]= label
    args[:title]||= args[:help]
    guidance=args.delete(:guidance)
    (write guidance; newline) if guidance
    list(getter, ID_Gen.next, args, &block)
    Document_Spacing_Context.last_widget_type= :lst
    gui_stack.pop
    if !table?
      newline; newline
    end
  end

=end

  # Block does not take an argument.
  def tbl(getter, label=nil, args = {}, &block)
    Document_Spacing_Context.reset
    label = generate_label(getter) unless label
    div(label) {
      gui_stack << :table
      args = args.clone
      args[:label]= label
      args[:title]||= args[:help]
      guidance=args.delete(:guidance)
      (write guidance; newline) if guidance
      table(getter, ID_Gen.next, args, &block)
    }
    Document_Spacing_Context.last_widget_type = :tbl
    gui_stack.pop
  end

  # Block does not take an argument.
  def lst(getter, label=nil, args = {}, &block)
    Document_Spacing_Context.reset
    label = generate_label(getter) unless label
    div(label) {
      gui_stack << :list
      args = args.clone
      args[:label]= label
      args[:title]||= args[:help]
      guidance=args.delete(:guidance)
      (write guidance; newline) if guidance
      list(getter, ID_Gen.next, args, &block)
    }
    Document_Spacing_Context.last_widget_type = :lst
    gui_stack.pop
  end


  def str(getter, label=nil, args={})
    label = generate_label(getter) unless label
    args = args.clone
    args[:label]||= label
    args[:title]||= args[:help]
    if table?
      string getter, args
    else
      newline_if_last_wiget_was_bulky
      write label+': '
      string getter, args
      newline
      guidance=args.delete(:guidance)
      (write guidance; newline) if guidance
    end
    Document_Spacing_Context.last_widget_type= :str
  end

  def int(getter, label=nil, args={})
    label = generate_label(getter) unless label
    args = args.clone
    args[:label]= label
    args[:title]||= args[:help]
    if table?
      integer getter, args
    else
      newline_if_last_wiget_was_bulky
      write label+': '
      integer getter, args
      newline
      guidance=args.delete(:guidance)
      (write guidance; newline) if guidance
    end
    Document_Spacing_Context.last_widget_type= :int
  end

  # New, not yet tested
  def flt(getter, label=nil, args={})
    label = generate_label(getter) unless label
    args = args.clone
    args[:label]= label
    args[:title]||= args[:help]
    if table?
      float getter, args
    else
      newline_if_last_wiget_was_bulky
      write label+': '
      float getter, args
      newline
      guidance=args.delete(:guidance)
      (write guidance; newline) if guidance
    end
    Document_Spacing_Context.last_widget_type= :int
  end

  def tf_dropdown(getter, label=nil, args={})
    label = generate_label(getter) unless label
    args = args.clone
    args[:label]= label
    args[:title]||= args[:help]
    if table?
      boolean getter, args
    else
      newline_if_last_wiget_was_bulky
      write label+': '
      boolean getter, args
      newline
      guidance=args.delete(:guidance)
      (write guidance; newline) if guidance
    end
    Document_Spacing_Context.last_widget_type= :tf_dropdown
  end

  def tf(getter, label=nil, args={})
    label = generate_label(getter) unless label
    args = args.clone
    args[:title]||= args[:help]
    if table?
      boolean_checkbox getter, args
    else
      # Note that we specify the label if not in a table. In a table, the column is labeled.
      args[:label]= label
      newline_if_last_wiget_was_bulky
      boolean_checkbox getter, args
      newline
      guidance=args.delete(:guidance)
      (write guidance; newline) if guidance
    end
    Document_Spacing_Context.last_widget_type= :tf
  end

  def rch(getter, label=nil, args={})
    label = generate_label(getter) unless label
    args = args.clone
    args[:label]= label
    args[:title]||= args[:help]
    args[:style]='full' unless args[:style]
    if table?
      richtext getter, args
    else
      newline if Document_Spacing_Context.last_widget_type
      write label+': '
      newline
      richtext getter, args
      guidance=args.delete(:guidance)
      (newline; write guidance) if guidance
      newline
    end
    Document_Spacing_Context.last_widget_type= :rch
  end

  def dat(getter, label=nil, args={})
    label = generate_label(getter) unless label
    args = args.clone
    args[:label]= label
    args[:title]||= args[:help]
    args[:style]='full' unless args[:style]
    if table?
      date getter, args
    else
      newline_if_last_wiget_was_bulky
      write label+': '
      date getter, args
      newline
      guidance=args.delete(:guidance)
      (write guidance; newline) if guidance
    end
    Document_Spacing_Context.last_widget_type= :dat
  end

  def chs(getter, label=nil, args={})
    label = generate_label(getter) unless label
    args = args.clone
    args[:label]= label
    args[:title]||= args[:help]
    if table?
      choice getter, nil, args
    else
      newline_if_last_wiget_was_bulky
      write label+': '
      choice getter, nil, args
      newline
      guidance=args.delete(:guidance)
      (write guidance; newline) if guidance
    end
    Document_Spacing_Context.last_widget_type= :chs
  end

  # An associated object (to-one association)
  def obj(getter, label=nil, args={}, &block)
    Document_Spacing_Context.reset
    args = args.clone
    args[:label]=label
    args[:title]||= args[:help]
    args[:list_style_type] = 'none'
    lst(getter, label, args, &block)
    Document_Spacing_Context.last_widget_type= :obj
  end


  def div(label, args={}, &block)
    return yield if table?
    Document_Spacing_Context.reset
    n = 1
    if !table?
      n = 2+gui_stack.size
    end
    header(n) {newline}
    gui_stack << :div
    aargs = args.clone
    args[:label]=label
    args[:title]||= args[:help]
    args[:classes]||= 'document_collapsable_context start_collapsed'
    args[:label_style]= {:header => n }
    context(nil,nil, args, &block)
    gui_stack.pop
    Document_Spacing_Context.last_widget_type= :div
  end

end
