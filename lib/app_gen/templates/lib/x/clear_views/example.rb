doc(Package::Classifier, 'Document Title (for doc selection dropdown)') {


  # Methods that are shown with one argument generally take a getter.
  # These methods will derive a label from the getter.
  # If you want a different label, you can pass it as an optional second argument.
  # A Hash passed as an optional third argument can be used to provide additional information.
  # Hash items may include:
  #    :association_class_widget => true # When value comes from an association class instance
  #    :help => hover_string
  #    :guidance => always_visible_string
  #    :list_style_type  (value can be 'none'. Not certain about other values, probably 'bullet', 'number)
  #    :label_style => {:header => 3 }

  # Size and centering adjustment
  header(1, 'text-align' => 'center') {
    newline
    str 'name'  # String
    }

  # Styled
   style('display' => 'block', 'text-align' => 'center') {
     newline
     file 'photo'  # Stored file
     image :getter => 'photo'  # Render the stored file as an image
   }

   tf  'is_consistent' # True/False (checkbox - lacks a "no value" choice)
   tf_dropdown 'is_consistent' # Boolean (3 valued dropdown - allows for no value)
   int 'num'  # Integer
   # flt 'float'  (this seems to be missing)
   dat 'date' # Date
   chs 'type' # Choice
   rch 'description' # RichText

=begin
  Example divider attribute_strings
  These examples are from https://codepen.io/ibrahimjabbari/pen/ozinB

   # Color, height, margin, width: 'style="color:blue; margin-left:30px; height: 5px; width: 80%"'
   # Double: 'style="border-top: 3px double #8c8b8b;"'
   # Dashed: 'style="border-top: 1px dashed #8c8b8b;"'
   # Dotted: 'style="border-top: 1px dotted #8c8b8b;"'
   # Vertical gradient: 'style="height: 10px; width: 80%; box-shadow: 0 10px 10px -10px #8c8b8b inset"'
   # Horizontal gradient:
   'style="border: 0; height: 1px;
     background-image: -webkit-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
     background-image: -moz-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
     background-image: -ms-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
     background-image: -o-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); "'
=end
   divider attribute_string # Argument is not a getter, and this method does not take additional arguments

   # =========================
   # The following are some pre-defined generic d3 widgets.
   # See the Argus project for examples.
   # The widget_name is embedded in the HTML. It has to be unique, but it's not very interesting, maybe should generate it.
   # You can easily impliment your own d3 widgets. See defintions of these methods in dsl_extension.r
   # =========================

   # These getters return JSON.
   sankey(getter, label, widget_name)
   xy(getter, label, selector_label, widget_name)

   # This lets you switch between different representations of trees. Representations include:
   #   * sunburst
   #   * zoomTree
   #   * radial_reingold_tilford
   # You can also change the order in which axes are traversed.
   # Getter returns a Hash
   tree_gor(getter, label, widget_name, unused_axes, default_axis_sequence)

   # =========================
   # End pre-defined generic d3 widgets.
   # =========================

   # List (getter returns a list)
   lst('included_features') {
     str 'name'
     rch 'description'
     # Table (getter returns a list). Labels (derived from getters if necessary) are used only in header.
     tbl( 'benefits') {
       str 'name'
       rch 'description'
     }
   }

   # Collapseable section.  Argument is not a getter.
   div('Label') {
      tbl('core_interests') { interest_content }
      obj('getter') { object_content }  # Use when getter returns a single object instead of a list. Otherwise use "lst" or "tbl"
    }


}
