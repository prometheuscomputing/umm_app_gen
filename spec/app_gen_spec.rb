require 'spec_helper'
# NOTE: when debugging these tests, $output may be printed to see any errors that occurred during plugin execution via: puts $output

# This uses the installed gems and ignores lodepath modifications
describe 'appGen' do
  before :all do
    # @dsl_file_path = File.expand_path('~/projects/uml_metamodel/test_data/CarExampleApplication.rb')
    @dsl_file_path = File.expand_path(File.join(__dir__, '../test_data/CarExample-2.7.2.rb'))
  end
  it "should not produce any errors" do
    return_value = UmmAppGenerator.construct_app_from_file(@dsl_file_path)
    expect(return_value).to eq ''
  end
  
  # This uses the installed gems and ignores lodepath modifications
  # it "should not produce any errors" do
  #   # message = system("generate_ruby_app #{@dsl_file_path}") # requires gem to be installed
  #   script_path = File.expand_path("#{__dir__}/../bin/generate_ruby_app")
  #   return_value = system("ruby #{script_path} #{@dsl_file_path}") # requires gem to be installed
  #   expect(return_value).to eq true
  # end
end
